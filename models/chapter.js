'use strict'

module.exports = {

    name: String,
    content: {
        _id: String,
        url: String
    },
    iconSelected: {
        _id: String,
        url: String
    },
    iconUnselected: {
        _id: String,
        url: String
    },
    paymentStatus: {
        type: Boolean,
        default: false
    },
    totalQuizQuestions: Number,
    correctQuizQuestions: Number,
    quizPercentage: String

}