'use strict'

module.exports = {
    chapterId: String,
    urlType: {
        type: String,
        enum: ['iconSelected', 'iconUnselected','content']
    },
    url: String
}