'use strict'

module.exports = {
    userId: String,
    chapterId: String,
    quizPercentage: String,
    answers:[{
            isCorrect:Boolean,
            questionId:'string',
            answerId:'string'
        }]
}