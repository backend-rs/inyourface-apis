'use strict'

module.exports = {
    chapterId:String,
    question: String,
    options: [{
        value: String
    }],
    answer: String
}