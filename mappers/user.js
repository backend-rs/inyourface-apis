'use strict'

// users create mapper
exports.toModel = entity => {

    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        phone: entity.phone
    }
    return model
}

// for verifyUser
exports.toVerifyUser = entity => {
    var model = {
        _id: entity._id,
        isVerified: entity.isVerified,
        type: entity.type,
        name: entity.name,
        email: entity.email,
        token: entity.token
    }
    if (entity.profile) {
        model.profile = {
            image: {
                url: entity.profile.image.url,
                thumbnail: entity.profile.image.thumbnail,
                resize_url: entity.profile.image.resize_url,
                resize_thumbnail: entity.profile.image.resize_thumbnail
            },
            bio: entity.profile.bio
        }
    }
    return model
}

// for login 
exports.toUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        token: entity.token,
        phone: entity.phone,
    }
    if (entity.profile) {
        model.profile = {
            image: {
                url: entity.profile.image.url,
                thumbnail: entity.profile.image.thumbnail,
                resize_url: entity.profile.image.resize_url,
                resize_thumbnail: entity.profile.image.resize_thumbnail
            },
            bio: entity.profile.bio
        }
    }
    return model
}

// for particular user
exports.toGetUser = entity => {
    var model = {
        _id: entity._id,
        type: entity.type,
        isVerified: entity.isVerified,
        name: entity.name,
        email: entity.email,
        chapters: entity.chapters
    }
    if (entity.profile) {
        model.profile = {
            image: {
                url: entity.profile.image.url,
                thumbnail: entity.profile.image.thumbnail,
                resize_url: entity.profile.image.resize_url,
                resize_thumbnail: entity.profile.image.resize_thumbnail
            },
            bio: entity.profile.bio
        }
    }
    return model
}

// for send token
exports.toSendToken = entity => {
    return {
        tempToken: entity.tempToken
    }
}