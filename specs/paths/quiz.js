module.exports = [{
        url: '/',
        get: {
            summary: 'Search',
            description: 'get quiz list',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },{
                in: 'query',
                name: 'id',
                description: 'chapterId',
                type: 'integer'
            }
        ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        },
        post: {
            summary: 'Create',
            description: 'Create Quiz',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of Quiz creation',
                    required: true,
                    schema: {
                        $ref: '#/definitions/quizCreateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get:{
            summary:'Get',
            description:'get user by Id',
            parameters:[{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in:'path',
                name:'id',
                description:'quizId',
                required:true,
                type:'string'
            }],
            responses:{
                default:{
                    description:'Unexpected error',
                    schema:{
                        $ref:'#/definitions/Error'
                    }
                }
            }
        }
    }
]