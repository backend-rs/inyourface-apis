module.exports = [{
        url: '/',
        get: {
            summary: 'Search',
            description: 'get chapters list',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        post: {
            summary: 'Create',
            description: 'Create chapter',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                //  {
                //     in: 'query',
                //     name: 'name',
                //     description: 'name',
                //     required: true,
                //     type: 'string'
                // }, 
                // {
                //     "name": "file",
                //     "in": "formData",
                //     "description": "please choose an file",
                //     "required": true,
                //     "type": "file"
                // },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of Chapter creation',
                    schema: {
                        $ref: '#/definitions/chapterCreateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'Get',
            description: 'get chapter by Id',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'path',
                    name: 'id',
                    description: 'questionId',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        put: {
            summary: 'Update chapter',
            description: 'update chapter details',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'path',
                    name: 'id',
                    description: 'chapterId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of chapter update',
                    required: true,
                    schema: {
                        $ref: '#/definitions/chapterUpdateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    }
]