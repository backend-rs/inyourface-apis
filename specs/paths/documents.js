module.exports = [{
    url: '/',
    get: {
        summary: 'Search',
        description: 'get documents list',
        parameters: [{
            in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'query',
            name: 'chapterId',
            description: 'chapterId',
            required: true,
            type: 'string'
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    },
    post: {
        summary: 'Upload document',
        description: 'Upload document',
        parameters: [{
            in: 'headers',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
        }, {
            in: 'query',
            name: 'chapterId',
            description: 'chapterId',
            required: true,
            type: 'string'
        }, {
            in: 'query',
            name: 'urlType',
            description: 'urlType',
            required: true,
            type: 'string'
        }, {
            "name": "file",
            "in": "formData",
            "description": "please choose a document",
            "required": true,
            "type": "file"
        }, {
            in: 'body',
            name: 'body',
            description: 'Model of document creation',
            required: true,
            schema: {
                $ref: '#/definitions/documentCreateReq'
            }
        }],
        responses: {
            default: {
                description: 'Unexpected error',
                schema: {
                    $ref: '#/definitions/Error'
                }
            }
        }
    }
}]