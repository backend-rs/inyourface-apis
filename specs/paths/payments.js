module.exports=[{
    url:'/token',
    get:{
        summary:'Get',
        description:'get token',
        parameters:[{
            in:'header',
            name:'x-access-token',
            description:'token to access api',
            required:true,
        }],
        responses:{
            default:{
                description:'Unexpected error',
                schema:{
                    $ref:'#/definitions/Error'
                }
            }
        }
    }
}]