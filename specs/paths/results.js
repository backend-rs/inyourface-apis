module.exports=[
    {
        url:'/',
        post:{
            summary:'Create',
            description:'Create Result',
            parameters:[{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            },
            {
                in:'body',
                name:'body',
                description:'Model of result creation',
                required:true,
                schema:{
                    $ref:'#/definitions/resultCreateReq'
                }
            }],
            responses:{
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }

}
]