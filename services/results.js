'use strict'
const service = require('../services/quiz')
const chapterService = require('../services/chapters')

const set = async (entity, model, context) => {
    const log = context.logger.start('services/results/set')
    try {
        if (model) {
            entity.quizPercentage = model
        }
        log.end()
        return entity
    } catch (err) {

        log.end()
        return new Error(err)
    }
}
const createTempResult = async (isCorrect, item, option) => {
    if (isCorrect == true) {
        var returnResult = {
            isCorrect: true,
            questionId: item.questionId,
            answerId: option.id,
            answerValue: option.value
        }
    } else {
        var returnResult = {
            isCorrect: false,
            questionId: item.questionId,
            answerId: option.id,
            answerValue: option.value
        }
    }

    return returnResult
}

const create = async (model, context) => {
    const log = context.logger.start('services/results/create')
    try {
        let result
        let chapter
        let isCorrect = Boolean
        let quizPercentage
        let totalQuestions
        let quiz
        let tempResult
        let tempResults = []
        let count
        let results
        if (model.answers) {
            for (const item of model.answers) {
                if (item) {
                    quiz = await db.quiz.findOne({
                        '_id': {
                            $eq: item.questionId
                        }
                    })
                    if (quiz) {
                        let correctOption
                        for (const option of quiz.options) {
                            if (option) {
                                if (option.id == item.answerId) {
                                    if (option.value == quiz.answer) {
                                        isCorrect = true
                                        correctOption = option

                                    } else {
                                        isCorrect = false
                                    }

                                }
                            }
                        }
                        if (isCorrect != true) {
                            for (const option of quiz.options) {
                                if (option) {
                                    if (option.value == quiz.answer) {
                                        isCorrect = false
                                        correctOption = option
                                    }
                                }
                            }
                        }
                        tempResult = await createTempResult(isCorrect, item, correctOption)
                        tempResults.push(tempResult)
                        console.log(tempResults)
                    }
                }

            }
            // total questions
            // totalQuestions = tempResults.length
            let request = {
                query: {
                    'id': model.chapterId
                }
            }
            quiz = await service.get(request, context)
            totalQuestions = quiz.length

            // correct questions
            count = 0;
            for (var i = 0; i < tempResults.length; i++) {
                if (tempResults[i].isCorrect == true) {
                    count++
                }
            }

            // total percentage
            quizPercentage = (count / totalQuestions) * 100

        }
        // find result of user if already exist for chapter
        result = await db.result.findOne({
            'userId': {
                $eq: context.user.id
            },
            'chapterId': {
                $eq: model.chapterId
            }
        })

        if (!result) {
            //if result not  exist
            result = await new db.result({
                userId: context.user.id,
                chapterId: model.chapterId,
                quizPercentage: quizPercentage
            }).save()
            if (quizPercentage == 0 || quizPercentage == undefined || quizPercentage == null) {
                result.quizPercentage = 0
            }
            result.answers = tempResults
        } else {
            // if exist
            result = await update(result.id, quizPercentage, context)
            result.answers = tempResults
            console.log(result)
        }

        // update quizPercentage in chapter
        let percentage = {
            correctQuizQuestions: count,
            quizPercentage: quizPercentage
        }
        chapter = await chapterService.update(model.chapterId, percentage, context)

        log.end()
        return result
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/results:${id}`)
    try {

        const entity = await db.result.findById(id)
        if (!entity) {
            throw new Error(err)
        }

        let result = await set(entity, model, context)
        log.end()
        return result.save()

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.update = update