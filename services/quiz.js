'use strict'

const create = async (model, context) => {
    const log = context.logger.start('services/quiz/create')
    try {
        
        let quiz = await new db.quiz(model).save()
        log.end()
        return quiz

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/quiz/get:${id}`)

    try {
        const quiz = await db.quiz.findById(id)
        log.end()
        return quiz
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`api/quiz/get`)
    try {
        let quiz
        let params = req.query

        // get questions through chapterId
        if (params && (params.id != undefined && params.id != null)) {
            quiz = await db.quiz.find({
                'chapterId': {
                    $eq: params.id
                }
            })
            log.end()

        } else {
            log.end()
            quiz = await db.quiz.find({})
        }
        log.end()
        return quiz

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

exports.create = create
exports.getById = getById
exports.get = get