'use strict'
const braintree = require("braintree");
// const service=require('./payment')

const payment = async (req, res) => {

    // all credentials
    const gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
        merchantId: "x266y63pjnx4k32w",
        publicKey: "bcj3jvsdn45ksxz5",
        privateKey: "dc27561bfeec8a2d4c18a3bdc93ebe37"
    });

    // Use the payment method nonce here
    const nonceFromTheClient = req.body.paymentMethodNonce;

    return new Promise((resolve, reject) => {
        gateway.transaction.sale({
            amount: '31.00',
            paymentMethodNonce: nonceFromTheClient,
            options: {
                submitForSettlement: true
            }
        }, async (error, result) => {
            if (result) {
                resolve(result)
                console.log(result)
            } else {
                reject(error)
            }
        });
    })
}

const create = async (req, context, res) => {
    const log = context.logger.start('services/payments/checkout')
    try {

        let paymentTemp = {}

        await payment(req, res).then(async (res) => {
            paymentTemp.transactionId = res.transaction.id
            console.log('ID:::', res.transaction.id)
            let payment = await new db.payment(paymentTemp).save()
            log.end()
            return payment
        }, error => {
            log.end()
            throw new Error(error)
        }).catch(error => {
            log.end()
            throw new Error(error)
        })
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const paymentToken = async (req, context, res) => {
    const log = context.logger.start('services/payments/paymentToken')

    // all credentials
    const gateway = braintree.connect({
        environment: braintree.Environment.Sandbox,
        merchantId: "x266y63pjnx4k32w",
        publicKey: "bcj3jvsdn45ksxz5",
        privateKey: "dc27561bfeec8a2d4c18a3bdc93ebe37"
    });

    return new Promise((resolve, reject) => {
        gateway.clientToken.generate({},
            async (error, response) => {
                if (response) {
                    const clientToken = {
                        token : response.clientToken
                    }
                    resolve(clientToken)
                    console.log(clientToken)
                    log.end()
                } else {
                    reject(error)
                    log.end()
                }

            });
    })


}

exports.payment = payment
exports.create = create
exports.paymentToken = paymentToken





// const token = gateway.clientToken.generate({}, function (err, response) {
//     var clientToken = response.clientToken
//     console.log(clientToken)
// });



































// <!DOCTYPE html>
// <html>
// <body>

// <script src="https://js.braintreegateway.com/web/dropin/1.20.1/js/dropin.min.js"></script>
// <script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>

// <div id="dropin-wrapper">
//   <div id="checkout-message"></div>
//   <div id="dropin-container"></div>
//   <button id="submit-button">Submit payment</button>
// </div>

// <script>
//   var button = document.querySelector('#submit-button');

//   braintree.dropin.create({
//     // Insert your tokenization key here
//     authorization: 'sandbox_pgcn8fzh_x266y63pjnx4k32w',
//     container: '#dropin-container'
//   }, function (createErr, instance) {
//     button.addEventListener('click', function () {
//       instance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
//         // When the user clicks on the 'Submit payment' button this code will send the
//         // encrypted payment information in a variable called a payment method nonce
//         $.ajax({
//           type: 'POST',
//           url: 'http://localhost:7000/api/chapters/checkout',
//           data: {'paymentMethodNonce': payload.nonce}
//         }).done(function(result) {
//           // Tear down the Drop-in UI
//           instance.teardown(function (teardownErr) {
//             if (teardownErr) {
//               console.error('Could not tear down Drop-in UI!');
//             } else {
//               console.info('Drop-in UI has been torn down!');
//               // Remove the 'Submit payment' button
//               $('#submit-button').remove();
//             }
//           });

//           if (result.isSuccess) {
//             $('#checkout-message').html('<h1>Success</h1><p>Your Drop-in UI is working! Check your <a href="https://sandbox.braintreegateway.com/login">sandbox Control Panel</a> for your test transactions.</p><p>Refresh to try another transaction.</p>');
//           } else {
//             console.log(result);
//             $('#checkout-message').html('<h1>Error</h1><p>Check your console.</p>');
//           }
//         });
//       });
//     });
//   });
// </script>

// </body>
// </html>