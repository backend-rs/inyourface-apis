'use strict'
const service = require('../services/users')
const documentService = require('../services/documents')
const quizService = require('../services/quiz')
const fileUpload = require('./documents')

const set = async (entity, model, context) => {
    const log = context.logger.start('services/chapters/set')
    try {
        let isChapter = false
        let user

        if (model.paymentStatus) {
            entity.paymentStatus = model.paymentStatus


            user = await db.user.findOne({
                '_id': {
                    $eq: context.user.id
                }
            })
            if (!user) {
                throw new Error(err)
            } else {
                for (const chapter of user.chapters) {
                    if (chapter.id == model._id) {
                        isChapter = true
                        throw new Error('chapter already added')

                    }
                }
                if (!isChapter == true) {
                    user = await service.update(context.user.id, entity, context)
                }
            }
        }
        if (model.quizPercentage) {
            entity.quizPercentage = model.quizPercentage
        }
        if (model.correctQuizQuestions) {
            entity.correctQuizQuestions = model.correctQuizQuestions
        }
        log.end()
        return entity

    } catch (err) {
        throw new Error(err)
    }

}
const create = async (model, context) => {
    const log = context.logger.start('services/chapters/create')
    try {
        let chapter = await new db.chapter(model).save()
        if (chapter.quizPercentage == undefined || chapter.quizPercentage == null) {
            chapter.quizPercentage = '0'
        }
        if (chapter.correctQuizQuestions == undefined || chapter.correctQuizQuestions == null) {
            chapter.correctQuizQuestions = 0
        }
        log.end()
        chapter.save()
        return chapter
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const getById = async (id, context) => {
    const log = context.logger.start(`services/chapters/get:${id}`)
    try {
        let chapter
        let document
        var req = {
            query: {
                'chapterId': id
            }
        }
        // find url for content
        document = await documentService.get(req, context)
        console.log(document)

        // find chapter
        chapter = await db.chapter.findById(id)
        console.log(chapter)
        for (const item of document) {
            if (item.urlType == 'content') {
                chapter.content._id = item.id,
                    chapter.content.url = item.url
            }
        }

        log.end()
        return chapter
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/chapters/get`)
    try {
        let chapters
        let document
        let totalQuiz

        // find all chapters
        chapters = await db.chapter.find({})

        for (const chapter of chapters) {
            if (chapter) {
                let request = {
                    query: {
                        'chapterId': chapter.id
                    }
                }
                // find url for icons
                document = await documentService.get(request, context)
                console.log(document)

                for (const item of document) {

                    if (item.urlType == 'content') {
                        chapter.content._id = item.id,
                            chapter.content.url = item.url
                    }
                    if (item.urlType == 'iconSelected') {
                        chapter.iconSelected._id = item.id,
                            chapter.iconSelected.url = item.url
                    }
                    if (item.urlType == 'iconUnselected') {
                        chapter.iconUnselected._id = item.id,
                            chapter.iconUnselected.url = item.url
                    }
                }

                // find totalQuizQuestions according to chapters from Quiz
                let quizQuery = {
                    query: {
                        'id': chapter.id
                    }
                }
                totalQuiz = await quizService.get(quizQuery, context)
                if (!totalQuiz) {
                    throw new Error('Quiz does not exist for this chapter')
                }
                chapter.totalQuizQuestions = totalQuiz.length
                console.log(totalQuiz)
            }
        }
        log.end()
        return chapters
    } catch (err) {
        log.end()
        throw new Error(err)
    }
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/chapters:${id}`)
    try {

        const entity = await db.chapter.findById(id)
        if (!entity) {
            throw new Error(err)
        }

        let chapter = await set(entity, model, context)
        log.end()
        return chapter.save()

    } catch (err) {
        log.end()
        throw new Error(err)
    }
}
exports.create = create
exports.getById = getById
exports.get = get
exports.update = update










// let params = req.query

// let file = await fileUpload.create(req, body, context)
// console.log(file)

// let chapterTemp = {
//     name: body.name,
//     content: {
//         _id: file.id,
//         url: file.url
//     }
// }

// if(document.length==0){
//     throw new Error('document not found')
// }


// paypal.configure({
//     'mode': 'sandbox', //sandbox or live
//     'client_id': 'AWM6chHC1rvRSRvRUNcjiOWiFyHNF62Wi_F2n_C9QWXkO_R_sA5o0KhgalQN3GGJqfLXosJkfqgsNfUJ',
//     'client_secret': 'ELTQ8N-ma2Q1-RS8URO6Ws4-FUGuOVR6KOTY2jjGxxMPiNQK3MJdeQMAUqIx6uVEjFFgI3rdID4CpLCn'
// });


// const payment = async (req) => {

//     const create_payment_json = {
//         "intent": "sale",
//         "payer": {
//             "payment_method": "paypal"
//         },
//         "redirect_urls": {
//             "return_url": "http://localhost:7000/success",
//             "cancel_url": "http://localhost:7000/cancel"
//         },
//         "transactions": [{
//             "item_list": {
//                 "items": [{
//                     "name": "iiiiiiii",
//                     "sku": "123",
//                     "price": "20.00",
//                     "currency": "USD",
//                     "quantity": 1
//                 }]
//             },
//             "amount": {
//                 "currency": "USD",
//                 "total": "20.00"
//             },
//             "description": "This is the payment description."
//         }]
//     };

//     paypal.payment.create(create_payment_json, function (error, payment) {
//         if (error) {
//             console.log('ffffffffffffffffffffffffff:', error)
//             throw error;
//         } else {
//             console.log("Create Payment Response");
//             console.log(payment);

//             for (let i = 0; i < payment.links.length; i++) {
//                 if (payment.links[i].rel == 'approval_url') {
//                     console.log(payment.links[i].href)
//                     res.redirect(payment.links[i].href)

//                 }
//             }

//             res.send('qqqqqqqqqqqqqqqqqqqqqqqqqqqqqq')
//         }
//     });


// }