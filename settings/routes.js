'use strict'

const fs = require('fs')
const specs = require('../specs')
const api = require('../api')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app, logger) => {
    const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

  app.get('/ch1', function (req, res) {
        fs.readFile('./public/ch2.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })


app.get('/files/documents/1.html', function (req, res) {
	fs.readFile('./public/files/documents/1.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/2.html', function (req, res) {
	fs.readFile('./public/files/documents/2.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/3.html', function (req, res) {
	fs.readFile('./public/files/documents/3.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/4.html', function (req, res) {
	fs.readFile('./public/files/documents/4.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/5.html', function (req, res) {
	fs.readFile('./public/files/documents/5.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/6.html', function (req, res) {
	fs.readFile('./public/files/documents/6.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/7.html', function (req, res) {
	fs.readFile('./public/files/documents/7.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

app.get('/files/documents/8.html', function (req, res) {
	fs.readFile('./public/files/documents/8.html', function (err, data) {
		if (err){
			return res.json({
				isSuccess: false,
				error:err.toString()
			})
		}
		res.contentType('text/html')
		res.send(data)
	})
})

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })


    // .......................users routes..............................
   
    app.get('/api/users', auth.context.builder, auth.context.requiresToken, api.users.get);

    app.post('/api/users/verifyUser', auth.context.builder, validator.users.verifyUser, api.users.verifyUser);
    app.post('/api/users', auth.context.builder, validator.users.canCreate, api.users.create);

    app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.getById, api.users.getById);
    app.put('/api/users/:id', auth.context.builder, auth.context.requiresToken, validator.users.update, api.users.update);
    app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login);

    app.post('/api/users/forgotPassword', auth.context.builder, validator.users.forgotPassword, api.users.forgotPassword);
    app.post('/api/users/resetPassword', auth.context.builder, validator.users.resetPassword, api.users.resetPassword);

    app.post('/api/users/changePassword', auth.context.builder, auth.context.requiresToken, validator.users.changePassword, api.users.changePassword);
    app.post('/api/users/logOut', auth.context.builder, auth.context.requiresToken, api.users.logOut)


    // .......................chapters routes..........................
    app.post('/api/chapters', auth.context.builder, auth.context.requiresToken, multipartMiddleware, api.chapters.create)
    app.get('/api/chapters/:id', auth.context.builder, auth.context.requiresToken, api.chapters.getById)
    app.get('/api/chapters', auth.context.builder, auth.context.requiresToken, api.chapters.get)
    app.put('/api/chapters/:id', auth.context.builder, auth.context.requiresToken, api.chapters.update)

    // ................................payments routes......................
    app.post('/api/payments/checkout', auth.context.builder, api.payments.create)
    app.post('/api/payments/pay', api.payments.payment)
    app.get('/api/payments/token', auth.context.builder, auth.context.requiresToken, api.payments.paymentToken)

    // ........................quiz routes...............................
    app.post('/api/quiz', auth.context.builder, validator.quiz.create, auth.context.requiresToken, api.quiz.create)
    app.get('/api/quiz/:id', auth.context.builder, validator.quiz.getById, auth.context.requiresToken, api.quiz.getById)
    app.get('/api/quiz', auth.context.builder, auth.context.requiresToken, api.quiz.get)

    // ........................result routes...................................
    app.post('/api/results', auth.context.builder, auth.context.requiresToken, api.results.create)

    // ................................upload htmlDocuments............................................
    app.post('/api/documents', auth.context.builder, multipartMiddleware, api.documents.create)
    app.post('/api/documents/upload', multipartMiddleware, api.documents.upload);
    app.get('/api/documents', auth.context.builder, multipartMiddleware, api.documents.get)


    // ................................upload files............................................
    app.post('/api/files', auth.context.builder, multipartMiddleware, api.files.create)
    app.post('/api/files/upload', multipartMiddleware, api.files.upload);
    app.get('/api/files/:id', auth.context.builder, api.files.getById)


    log.end()
}
exports.configure = configure
