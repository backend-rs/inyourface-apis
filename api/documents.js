'use strict'

const response = require('../exchange/response')
const service = require('../services/documents')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/documents`)
    try {
        const document = await service.create(req, req.body, req.context)
        log.end()
        return response.data(res, document)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'document already exists')
    }

}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/documents/get`)
    try {
        const document = await service.get(req,req.context)
        log.end()
        return response.data(res, document)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.upload = async (req, res) => {

    try {
        let data = await service.upload(req.files.file)

        return response.data(res, data)
    } catch (err) {

        return response.failure(res, err.message)
    }

}