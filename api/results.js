'use strict'

const response = require('../exchange/response')
const service = require('../services/results')
const mapper = require('../mappers/result')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/result/create`)
    try {
        const result = await service.create(req.body, req.context)
        log.end()
        return response.data(res, result)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)

    }
}
exports.update = async (req, res) => {
    const log = req.context.logger.start(`api/results/${req.params.id}`)
    try {
        const result = await service.update(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, result)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)

    }
}