'use strict'

const response = require('../exchange/response')
const service = require('../services/chapters')
const mapper = require('../mappers/chapter')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/chapters/create`)
    try {
        const chapter = await service.create(req.body, req.context)
        log.end()
        return response.data(res, chapter)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Chapter already exists')
    }

}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/chapters/get/${req.params.id}`)
    try {
        const chapter = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, chapter)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/chapters/get`)
    try {
        const chapter = await service.get(req,req.context)
        log.end()
        return response.data(res, chapter)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.update = async (req, res) => {
    const log = req.context.logger.start(`api/chapters/${req.params.id}`)
    try {
        const chapter = await service.update(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, chapter)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}