'use strict'

const response = require('../exchange/response')
const service = require('../services/quiz')
const mapper = require('../mappers/quiz')

exports.create = async (req, res) => {
    const log = req.context.logger.start(`api/quiz/create`)
    try {
        const quiz = await service.create(req.body, req.context)
        log.end()
        return response.data(res, quiz)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Quiz already exists')
    }

}

exports.getById = async (req, res) => {
    const log = req.context.logger.start(`api/quiz/get/${req.params.id}`)

    try {
        const quiz = await service.getById(req.params.id, req.context)
        log.end()
        return response.data(res, quiz)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.get = async (req, res) => {
    const log = req.context.logger.start(`api/quiz/get`)

    try {
        const quiz = await service.get(req, req.context)
        log.end()
        return response.data(res, quiz)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}